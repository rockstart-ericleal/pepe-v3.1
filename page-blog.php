<?php
/* 

Template Name: Blog 
*/ 
get_header();
?>

<?php get_template_part( 'template-parts/content', 'menu' ); ?>
<?php get_template_part( 'template-parts/content', 'blog' ); ?>

<?php
get_footer();