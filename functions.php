<?php
/**
 * Pepe Davalos V3 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Pepe_Davalos_V3
 */

function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

if ( ! function_exists( 'pepe_davalos_v3_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function pepe_davalos_v3_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Pepe Davalos V3, use a find and replace
		 * to change 'pepe-davalos-v3' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'pepe-davalos-v3', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'MENU PEPE DAVALOS', 'pepe-davalos-v3' ),
		) );
		

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'pepe_davalos_v3_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'pepe_davalos_v3_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function pepe_davalos_v3_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'pepe_davalos_v3_content_width', 640 );
}
add_action( 'after_setup_theme', 'pepe_davalos_v3_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pepe_davalos_v3_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'pepe-davalos-v3' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'pepe-davalos-v3' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'pepe_davalos_v3_widgets_init' );


// Register Custom Post Type
function joya_post_type() {

	$labels = array(
		'name'                  => _x( 'Joyas', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Joya', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Joyas', 'text_domain' ),
		'name_admin_bar'        => __( 'Joya', 'text_domain' ),
		'archives'              => __( 'Joya', 'text_domain' ),
		'attributes'            => __( 'Atributos de Joya', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Agregar nueva Joya', 'text_domain' ),
		'add_new'               => __( 'Nueva Joya', 'text_domain' ),
		'new_item'              => __( 'Nueva joya', 'text_domain' ),
		'edit_item'             => __( 'Editar joya', 'text_domain' ),
		'update_item'           => __( 'Actualizar joya', 'text_domain' ),
		'view_item'             => __( 'Ver Joya', 'text_domain' ),
		'view_items'            => __( 'Ver Joyas', 'text_domain' ),
		'search_items'          => __( 'Buscar Joyas', 'text_domain' ),
		'not_found'             => __( 'Joya no encontrada', 'text_domain' ),
		'not_found_in_trash'    => __( 'Joya no encontrada en papelera', 'text_domain' ),
		'featured_image'        => __( 'Imagen Joya', 'text_domain' ),
		'set_featured_image'    => __( 'Cargar Imagen Joya', 'text_domain' ),
		'remove_featured_image' => __( 'Remover imagen de Joya', 'text_domain' ),
		'use_featured_image'    => __( 'Usar Imagen de Joya', 'text_domain' ),
		'insert_into_item'      => __( 'Ingresar dentro', 'text_domain' ),
		'uploaded_to_this_item' => __( 'actualizar este item', 'text_domain' ),
		'items_list'            => __( 'Lista de joyas', 'text_domain' ),
		'items_list_navigation' => __( 'Navegar en lista', 'text_domain' ),
		'filter_items_list'     => __( 'Filtrar por joyas', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Joya', 'text_domain' ),
		'description'           => __( 'Lista de Joyas', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'thumbnail', 'page-attributes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-star-filled',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'post_type_joya', $args );

}
add_action( 'init', 'joya_post_type', 0 );



// Register Custom Post Type
function revista_post_type() {

	$labels = array(
		'name'                  => _x( 'Revistas', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Revista', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Revistas', 'text_domain' ),
		'name_admin_bar'        => __( 'Revista', 'text_domain' ),
		'archives'              => __( 'Revista', 'text_domain' ),
		'attributes'            => __( 'Atributos de Revista', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Agregar Nueva Revista', 'text_domain' ),
		'add_new'               => __( 'Nueva Revista', 'text_domain' ),
		'new_item'              => __( 'Nueva Revista', 'text_domain' ),
		'edit_item'             => __( 'Editar Revista', 'text_domain' ),
		'update_item'           => __( 'Actualizar Revista', 'text_domain' ),
		'view_item'             => __( 'Ver Revista', 'text_domain' ),
		'view_items'            => __( 'Ver Revistas', 'text_domain' ),
		'search_items'          => __( 'Buscar Revistas', 'text_domain' ),
		'not_found'             => __( 'Revista no encontrada', 'text_domain' ),
		'not_found_in_trash'    => __( 'Revista no encontrada en papelera', 'text_domain' ),
		'featured_image'        => __( 'Imagen Revista', 'text_domain' ),
		'set_featured_image'    => __( 'Cargar Imagen Revista', 'text_domain' ),
		'remove_featured_image' => __( 'Remover imagen de Revista', 'text_domain' ),
		'use_featured_image'    => __( 'Usar Imagen de Revista', 'text_domain' ),
		'insert_into_item'      => __( 'Ingresar dentro', 'text_domain' ),
		'uploaded_to_this_item' => __( 'actualizar este item', 'text_domain' ),
		'items_list'            => __( 'Lista de Revistas', 'text_domain' ),
		'items_list_navigation' => __( 'Navegar en lista', 'text_domain' ),
		'filter_items_list'     => __( 'Filtrar por Revistas', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Revista', 'text_domain' ),
		'description'           => __( 'Lista de revista', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'thumbnail', 'page-attributes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'post_type_revista', $args );

}
add_action( 'init', 'revista_post_type', 0 );



if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5e438e162a67f',
	'title' => 'Campos Joyas',
	'fields' => array(
		array(
			'key' => 'field_5e438f06febe8',
			'label' => 'Portada de Joya',
			'name' => 'portada_de_joya',
			'type' => 'image',
			'instructions' => 'Cargar la imagen de Joya',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e438e2ebf5a5',
			'label' => 'Texto Español',
			'name' => 'text_es',
			'type' => 'text',
			'instructions' => 'Ingresa la descripción en español',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'Nueva Descripción',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5e438e66bf5a6',
			'label' => 'Texto Ingles',
			'name' => 'texto_en',
			'type' => 'text',
			'instructions' => 'Ingresa la descripción en Ingles',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'New Description',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5e438f4ff67c4',
			'label' => 'Grupo',
			'name' => 'grupo',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'ANILLOS' => 'ANILLOS',
				'ARETES' => 'ARETES',
				'COLLARES' => 'COLLARES',
				'PULSERAS' => 'PULSERAS',
			),
			'default_value' => array(
				0 => 'ANILLOS',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post_type_joya',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5e43900e2b821',
	'title' => 'Campos Revistas',
	'fields' => array(
		array(
			'key' => 'field_5e4390149c96b',
			'label' => 'AÑO',
			'name' => 'anio',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				2009 => '2009',
				2010 => '2010',
				2011 => '2011',
				2012 => '2012',
				2013 => '2013',
				2014 => '2014',
				2015 => '2015',
				2016 => '2016',
				2017 => '2017',
				2018 => '2018',
				2019 => '2019',
				2020 => '2020',
				2021 => '2021',
				2022 => '2022',
				2023 => '2023',
				2024 => '2024',
			),
			'default_value' => array(
				0 => 2020,
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_5e4390729c96c',
			'label' => 'IMAGEN 1',
			'name' => 'imagen_1',
			'type' => 'image',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e4390949c96d',
			'label' => 'IMAGEN 2',
			'name' => 'imagen_2',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e4390b09c96e',
			'label' => 'IMAGEN 3',
			'name' => 'imagen_3',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e4390c29c96f',
			'label' => 'IMAGEN 4',
			'name' => 'imagen_4',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e4390d09c970',
			'label' => 'IMAGEN 5',
			'name' => 'imagen_5',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e4390e39c971',
			'label' => 'IMAGEN 6',
			'name' => 'imagen_6',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e4390f29c972',
			'label' => 'IMAGEN 7',
			'name' => 'imagen_7',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e4391039c973',
			'label' => 'IMAGEN 8',
			'name' => 'imagen_8',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e4391289c975',
			'label' => 'IMAGEN 9',
			'name' => 'imagen_9',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5e4391199c974',
			'label' => 'IMAGEN 10',
			'name' => 'imagen_10',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post_type_revista',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;



function custom_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        // 'featured_image' => 'Image',
        // 'title' => 'Title',
        // 'anio' => 'Año',
        // 'comments' => '<span class="vers"><div title="Comments" class="comment-grey-bubble"></div></span>',
        //'date' => 'Date'
     );
    return $columns;
}
add_filter('manage_post_type_joya_posts_columns' , 'custom_columns');

// function custom_columns_data( $column, $post_id ) {
//     switch ( $column ) {
//     case 'featured_image':
//         the_post_thumbnail( 'thumbnail' );
//         break;
//     }
// }
// add_action( 'manage_post_type_joya_custom_column' , 'custom_columns_data', 10, 2 ); 





/*
 * Add columns to exhibition post list
 */
 function add_acf_columns ( $columns ) {
   return array_merge ( $columns, array ( 
     'portada_de_joya' => __ ( 'Joya' ),
     'grupo' => __ ( 'Grupo' ),
     'text_es' => __ ( 'Description' ),
   ) );
 }
 add_filter ( 'manage_post_type_joya_posts_columns', 'add_acf_columns' );

/*
 * Add columns to exhibition post list
 */
 function exhibition_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'portada_de_joya':
       //echo get_post_meta ( $post_id, 'portada_de_joya', true );
       echo '<img src="'.get_field('portada_de_joya')['url'].'" style="width:100px"/>';
       break;
     case 'text_es':
       //echo get_post_meta ( $post_id, 'portada_de_joya', true );
       echo the_field('text_es');
       break;
     case 'grupo':
       //echo get_post_meta ( $post_id, 'portada_de_joya', true );
       echo the_field('grupo');
       break;
   }
 }
 add_action ( 'manage_post_type_joya_posts_custom_column', 'exhibition_custom_column', 10, 2 );




/**
* Removes width and height attributes from image tags
*
* @param string $html
*
* @return string
*/
function remove_image_size_attributes( $html ) {
return preg_replace( '/(width|height)="\d*"/', '', $html );
}

// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );

// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );




function custom_columns_revistas( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        // 'featured_image' => 'Image',
        // 'title' => 'Title',
        // 'anio' => 'Año',
        // 'comments' => '<span class="vers"><div title="Comments" class="comment-grey-bubble"></div></span>',
        //'date' => 'Date'
     );
    return $columns;
}
add_filter('manage_post_type_revista_posts_columns' , 'custom_columns_revistas');


/*
 * Add columns to exhibition post list
 */
 function add_acf_columns_revistas ( $columns ) {
   return array_merge ( $columns, array ( 
     'portada' => __ ( 'Portada' ),
     'anio' => __ ( 'AÑO' )
   ) );
 }
 add_filter ( 'manage_post_type_revista_posts_columns', 'add_acf_columns_revistas' );


/*
 * Add columns to exhibition post list
 */
 function exhibition_custom_column_revistas ( $column, $post_id ) {
   switch ( $column ) {
     case 'portada':
       //echo get_post_meta ( $post_id, 'portada_de_joya', true );
       echo '<img src="'.get_field('imagen_1')['url'].'" style="width:100px"/>';
       break;
     case 'anio':
       //echo get_post_meta ( $post_id, 'portada_de_joya', true );
       echo the_field('anio');
       break;
   }
 }
 add_action ( 'manage_post_type_revista_posts_custom_column', 'exhibition_custom_column_revistas', 10, 2 );
















/**
 * Enqueue scripts and styles.
 */
function pepe_davalos_v3_scripts() {
	// wp_enqueue_style( 'pepe-davalos-v3-style', get_template_directory_uri().'/css/styles.css' );

	// wp_enqueue_script( 'pepe-davalos-v3-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	// wp_enqueue_script( 'pepe-davalos-v3-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }
}
add_action( 'wp_enqueue_scripts', 'pepe_davalos_v3_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

