<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Pepe_Davalos_V3
 */

get_header();
get_template_part( 'template-parts/content', 'menu' ); 
get_template_part( 'template-parts/content', get_post_type() );
get_footer();

