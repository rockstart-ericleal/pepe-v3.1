<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Pepe_Davalos_V3
 */

?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/styles.css?v=0.0.23">

	<title>Pepe Dávalos</title>
	<style type="text/css">.spinner{width: 80px; height: 80px; border: 2px solid #f3f3f3; border-top:3px solid #515F5F; border-radius: 100%; position: absolute; top:0; bottom:0; left:0; right: 0; margin: auto; animation: spin 1s infinite linear; } @keyframes spin {from{transform: rotate(0deg); }to{transform: rotate(360deg); } } </style>

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/photoswipe.css">
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/photoswipe.min.js?v=0.3"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/photoswipe-ui-default.min.js?V=0.2"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/default-skin/default-skin.css">


</head>
<body class="position-relative" onload="loadOK()">

	<div id="box-preload" class="preload h-full bg-light w-100 fixed-top">
		<div class="spinner"></div>
	</div>


	<div class="bg-wrap">
        <div class="bg" style="background-image: url('<?php echo get_template_directory_uri()?>/img/bg-pepe-cover.jpg');"></div>
    </div>


