<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Pepe_Davalos_V3
 */




	
$text_page = array();

if ( strpos(get_locale(),'es') !== false ){
	$text_page = array(
		'text-1' => 'Pepe Dávalos ha buscado marcar una diferencia en las líneas y proyectos nuevos desde 2017, a través de diseñar piezas únicas con gemas de color de muy alta calidad, pero no necesariamente utilizadas por todos los joyeros.',

		'text-2' => 'José Dávalos Huerta es la tercera generación de una familia de gemólogos y joyeros, fundadores de una de las empresas mexicanas más prestigiosas en la importación de piedras preciosas y fabricación de alta joyería.',
		'text-3' => 'Siguiendo la pasión familiar, se gradúa como gemólogo en el Gemolical Institute of America, (GIA, “in campus”) en Vicenza, Italia, en el año 2000. Posteriormente, se titula en Administración de Empresas en la Universidad Iberoamericana con el objetivo de participar como socio y director dentro de la empresa.',
		'text-4' => 'En 2004 se incorpora a JD Joyeros como fundador y director general de C.A.E, representante de la marca S.T Dupont a nivel nacional; en 2008 funda IDC (International Diamond Company) y en 2012 importó a México la marca Champion Watch. A partir de 2014 toma la dirección general de la compañía, manteniéndola como una de las más importantes y de mayor prestigio en su ramo, incorporándola a la nueva era digital, colocándola así a la vanguardia de las joyerías mundiales.',

		'text-5' => 'José Dávalos ha colaborado en varias revistas con información, recomendaciones y tips de joyas y piedras preciosas, también ha participado en capacitaciones para diversas joyerías, marcas internacionales de joyería, relojería y accesorios; tiendas departamentales y cadenas de empeño.',
		'text-6' => 'Actualmente dirige con éxito las diferentes áreas de JD Joyeros, inaugurando en el 2014 el showroom privado de la compañía, donde vende y diseña joyería exclusiva a la medida y gusto del cliente, elaborada por artesanos que llevan varias generaciones trabajando en los talleres de JD Joyeros.'
	);	
}else{
	$text_page = array(
		'text-1' => 'XXX',

		'text-2' => 'XXX',
		'text-3' => 'XXX',
		'text-4' => 'XXX',

		'text-5' => 'XXX',
		'text-6' => 'XXX'
	);
}



get_header();
?>

<?php get_template_part( 'template-parts/content', 'menu-fixed' ); ?>


<div id="box-init" class="fixed-top v-100 h-full box-menu-absolute d-md-none d-none d-lg-block">
	
	<div class="row h-full">
		<div class="col-7">
			<div class="row h-full position-relative">
				
				<div class="col-12 p-0 align-self-center text-center">
					<img class="text-logo main-logo" data-aos="fade-up" src="<?php echo get_template_directory_uri()?>/img/pepe-title.svg">
					<p><?php echo $text_page['text-1']?></p>
				</div>				
			</div>		
		</div>
	</div>
	
</div>


<!-- HEAD -->
<div id="cover" class="section container-fluid no-gutters">
	<div class="row h-full">			
		<div class="col-12 align-self-center text-center  text-center">
			<img class="text-logo" src="<?php echo get_template_directory_uri()?>/img/pepe-title.svg">
			<div class="menu-lang text-center face-cr position-relative">
				<ul class="list-inline">
					<li class="list-inline-item">
						<span class="border-right-lang"><a href="#">ESP</a></span>
					</li>
					<li class="list-inline-item">
						<span class=""><a href="#">ENG</a></span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>



<div id="box-item-container" class="section container-fluid no-gutters position-relative">		
	<!-- IMG PEPE 1 -->
	<section >
		<div class="row h-full item-view">
			<div class="col">
				<div class="row">
					<div class="col-md-12 col-lg-7 d-md-none d-none d-lg-block h-full item-view position-relative">
						
						<!-- MENU FLOAT -->

						<!-- SPACE -->

						<!-- END MENU FLOAT -->
						
					</div>
					<div class="col-md-12 col-lg-5 h-full item-view">
						<div class="row h-100p">
							
							<p class="description-menu-right text-center pl-5 pr-5 d-block d-lg-none mb-5">Pepe Dávalos ha buscado marcar una diferencia en las líneas y proyectos nuevos desde 2017, a través de diseñar piezas únicas con gemas de color de muy alta calidad, pero no necesariamente utilizadas por todos los joyeros.</p>
							<div class="col-12 p-0 align-self-center text-center d-block d-lg-none">
								<img data-aos="fade-up" class="photo-pepe" src="<?php echo get_template_directory_uri()?>/img/pepe-davalos-1.jpg">
							</div>

							<div class="col-12 align-self-center p-0 text-right d-none d-lg-block">
								<img data-aos="fade-up" class="photo-pepe-img-1" src="<?php echo get_template_directory_uri()?>/img/pepe-index-1.png">
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- SEMBLANZA PEPE 1 -->
	<section>
		<div class="row  item-view ">
			<div class="col">
				<div class="row">
					<div class="col-md-12 col-lg-7 d-md-none d-none d-lg-block  item-view position-relative">
						
						<!-- MENU FLOAT -->

						<!-- SPACE -->

						<!-- END MENU FLOAT -->
						
					</div>
					<div class="col-md-12 col-lg-5  item-view my-5">
						<div class="row h-100p">
							<div class="col-12 align-self-center">
								<p class="description-menu-right text-center pl-5 pr-5"><?php echo $text_page['text-2']?></p>
								<p class="description-menu-right text-center pl-5 pr-5"><?php echo $text_page['text-3']?></p>
								<p class="description-menu-right text-center pl-5 pr-5"><?php echo $text_page['text-4']?></p>										
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- IMG PEPE 2 -->
	<section>
		<div class="row  item-view">
			<div class="col">
				<div class="row">
					<div class="col-md-12 col-lg-7 d-md-none d-none d-lg-block  position-relative">
						
						<!-- MENU FLOAT -->

						<!-- SPACE -->

						<!-- END MENU FLOAT -->

					</div>
					<div class="col-md-12 col-lg-5  bg-violet mb-4">
						<div class="row ">
							
							<div class="col-12 p-0 align-self-center text-center d-block d-lg-none">
								<img data-aos="fade-up" class="photo-pepe" src="<?php echo get_template_directory_uri()?>/img/pepe-davalos-2.jpg">
							</div>

							<div class="col-12 align-self-center p-0 text-right d-none d-lg-block">
								<img data-aos="fade-up" class="photo-pepe-img-1" src="<?php echo get_template_directory_uri()?>/img/pepe-index-2.png">
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- SEMBLANZA 2 -->
	<section>
		<div class="row  item-view ">
			<div class="col">
				<div class="row">
					<div class="col-md-12 col-lg-7 d-md-none d-none d-lg-block  position-relative">
						
						<!-- MENU FLOAT -->

						<!-- SPACE -->

						<!-- END MENU FLOAT -->

					</div>
					<div class="col-md-12 col-lg-5 mb-5">
						<div class="row h-100p mb-5">
							<div class="col-12 align-self-center text-center">										
								<p class="description-menu-right text-sm-center text-lg-justify pl-5 pr-5"><?php echo $text_page['text-5']?></p>
								<p class="description-menu-right text-sm-center text-lg-justify pl-5 pr-5"><?php echo $text_page['text-6']?></p>
								<img  class="signature mt-5" src="<?php echo get_template_directory_uri()?>/img/firma.svg">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php

get_footer();
