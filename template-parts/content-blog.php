<section class="container ">
	<div class="row justify-content-center pt-2 pt-lg-5">
		<div class="col-12 text-center my-4">
			<h1 class="face-cr">BLOG</h1>
		</div>

		<div class="col-9 col-md-12">
			
			<div class="row justify-content-center">


				<?php

				global $post;

			    $myposts = get_posts( array(
			        'posts_per_page' => 30,
			    ) );

			    if ( $myposts ) {
				    foreach ( $myposts as $post ) {
				        setup_postdata( $post ); 
				    	?>
				    	<div class="col-12 col-md-4 col-lg-3 d-flex justify-content-center mb-2">
							<div class="box-image mb-4 mb-lg-5 ">
								<a href="<?php the_permalink(); ?>">
									<?php echo get_the_post_thumbnail( $post_id, 'full', array( 'class' => 'item-image-cat' ) ); ?>							
									<h1 class="text-center mt-4 px-2 px-lg-3"><?php the_title(); ?></h1>
								</a>
							</div>
						</div>
				    	<?php
				    }
				    wp_reset_postdata();
				}else{
					?>No hay publicaciones<?php
				}
			?>

				
				
			</div>

		</div>
	</div>
</section>	