<!-- MENU WORDPRESS -->
<nav class="navbar navbar-expand-lg  fixed-top navbar-light ">

<div class="bg-wrap-movil">
    <div class="bg" style="background-image: url('<?php echo get_template_directory_uri()?>/img/bg-pepe-cover.jpg');"></div>
</div>
  
<div class="line-menu d-block d-lg-none"></div>
  <a class="navbar-brand" href="#">
  	<span class="d-block d-lg-none face-cr" style="font-size: 22px">PEPE DAVALOS</span>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <img id="list-menu" class="list-menu m-1" src="<?php echo get_template_directory_uri()?>/img/menu-button.svg">
  </button>

  	<?php

		wp_nav_menu( array(
		    'theme_location'  => 'menu-1',
		    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
		    'container'       => 'div',
		    'container_class' => 'collapse navbar-collapse face-cr text-center',
		    'container_id'    => 'navbarSupportedContent',
		    'menu_class'      => 'navbar-nav mr-auto mb-4 m-lg-0',
		    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
		    'walker'          => new WP_Bootstrap_Navwalker(),
		) );

	?>

  

<div class="bg-wrap-movil">
    <div class="bg" style="background-image: url('<?php echo get_template_directory_uri()?>/img/bg-pepe-cover.jpg');"></div>
</div>

</nav>
