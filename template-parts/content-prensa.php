<?php
	
	$args = array(
	    'post_type'=> 'post_type_revista',
	    'posts_per_page' => -1
	);

	$revistas_pepe = query_posts( $args );

	$year_tabs_pepe = array();
	$cover_img_pepe = array();





	foreach ($revistas_pepe as $key => $value) {

		$temp_content = array();

		$prensa_year = get_field( "anio", $value->ID );
		array_push($year_tabs_pepe,  $prensa_year);
		$list_images = array();

		$temp_content = array('year'=>$prensa_year);



		$sizes = '';
		$images = '';
		$cover = '';
		for ($i=1; $i < 8; $i++) { 
			$image = get_field( "imagen_".$i, $value->ID );
			
			$size = '2048x2048';			
			$width = $image['sizes'][ $size . '-width' ];
			$height = $image['sizes'][ $size . '-height' ];
			// echo $width;
			// echo $height;

			if ( !empty($image)){
				$images .= $image['url'].',';
				$sizes .= $width.'X'.$height.',';
			}

			if ($i == 1){
				$cover = $image;
			}
		}

		$images = rtrim($images,",");
		$sizes = rtrim($sizes,",");
		$temp_content["images"] = $images;
		$temp_content["cover"] = $cover;
		$temp_content["sizes"] = $sizes;


		array_push($cover_img_pepe, $temp_content, $sizes);

	}


	$year = date("Y");
	$global_years = array();
	for ($i=0; $i < 12; $i++) { 
		array_push($global_years, $year-$i);
	}


	function get_tabs_content($cover_img_pepe,$global_years){

		// print_r($global_years);
		// print_r($cover_img_pepe);

		$wrapper_content = '';
		foreach ($global_years as $key => $year) {



			$image_internal = '';
			foreach ($cover_img_pepe as $key => $revista) {				
				if( $year == $revista['year']){

					$list_imgs = $revista['images'];
					$sizes = $revista['sizes'];					

					$cover = $revista['cover']['url'];
					$image_internal .= <<<HTML
					<div id="img-$year-$key" onclick="openPhotoSwipe('$list_imgs','$sizes')" class="col-6 col-md-4 col-lg-3 p-2 p-md-3 text-center">
						<img class="img-publish-pepe" src="$cover">
					</div>
					HTML;
				}
			}


			foreach($cover_img_pepe as $key => $revista){
				
				$compi=($year==$revista['year']);
				if( $compi ){

					$active = '';
					if ( $key == 0 ){
						$active = ' show active';
					}


					$wrapper_content .= <<<HTML
					<div class="tab-pane fade $active" id="$year" role="tabpanel" aria-labelledby="$year-tab">
						<div class="container">
							<div class="row justify-content-center">

								$image_internal

							</div>	
						</div>
					</div>
					HTML;
				}
			}


		}
		
		return $wrapper_content;

	}


	function get_tabs_years($year_tabs_pepe = array()){


		$main_menu_index = '';
		$year_tabs_pepe = array_unique($year_tabs_pepe);
		$year_tabs_pepe = array_reverse($year_tabs_pepe);

		foreach ($year_tabs_pepe as $key => $year) {
			
			$aria_selected = 'false';
			$active = '';
			if ( count($year_tabs_pepe)-1 == $key ){
				$active = ' active';
			}

			if ( count($year_tabs_pepe)-1 == $key ){
				$aria_selected = 'true';
			}
			//<a class="nav-link $active" id="$year-tab" data-toggle="tab" href="#$year" role="tab" aria-controls="$year" aria-selected="$aria_selected">$year</a>
			$main_menu_index .= <<<HTML
			<li class="nav-item">
				
			</li>
			HTML;

				
		}

		return $main_menu_index;
	}



?>



<?php
	
	$text_page = array();
	
	if ( strpos(get_locale(),'es') !== false ){
		$text_page = array(
			'prensa' => 'PRENSA'
		);	
	}else{
		$text_page = array(
			'prensa' => 'MEDIA'
		);
	}

?>


<section class="container ">
	<div class="row justify-content-center pt-2 pt-lg-5">
		<div class="col-12 text-center my-4">
			<h1 class="face-cr"><?php echo $text_page['prensa'] ?></h1>
		</div>

		<div class="col-md-12">
			
			<section >
				<div class="row">
					<div class="col-12">
						<div class="container">
							<ul class="nav nav-tabs my-4" id="myTab" role="tablist">
								<!-- <li class="nav-item">
									<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">2015</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">2016</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="true">2017</a>
								</li> -->
								<?php echo get_tabs_years($year_tabs_pepe) ?>

							</ul>

							<div class="tab-content mb-5" id="myTabContent">
								
								<?php echo get_tabs_content($cover_img_pepe,$global_years) ?>							
<!-- 
								<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">B</div>
								<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">.C</div> -->
							</div>
						</div>
					</div>

				</div>
			</section>

		</div>
	</div>
</section>	



<!-- SECTION GALERY -->
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <!-- <button class="pswp__button pswp__button--share" title="Share"></button> -->

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

           <!--  <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div> -->

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
<!-- END SECTION GALERY -->