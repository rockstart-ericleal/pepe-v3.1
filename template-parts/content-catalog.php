<?php
	
	$grupo = basename(get_permalink());
	
	if ($grupo == 'rings'){
		$grupo = 'anillos';
	}

	if ($grupo == 'bracelets'){
		$grupo = 'pulseras';
	}

	if ($grupo == 'earrings'){
		$grupo = 'aretes';
	}

	if ($grupo == 'necklaces'){
		$grupo = 'collares';
	}
	
?>

<section class="container ">
	<div class="row pt-2 pt-lg-5">
		<div class="col-12 text-center my-4">
			<h1 class="face-cr text-uppercase"><?php echo $grupo?></h1>
		</div>

		<div class="col-12">
			
			<div class="row ">
				
				<?php 
				    // WP_Query arguments
				    $args = array (
				      'post_type'              => array( 'post_type_joya' ),
				      'post_status'            => array( 'publish' ),
				      'nopaging'               => true,
				      'meta_key'    => 'grupo',
				      'meta_value'  => $grupo
				    );


				    // The Query
				    $services = new WP_Query( $args );
				    $total_events = $services->found_posts;

				    // The Loop
				    if ( $services->have_posts() ) {
				    	while ( $services->have_posts() ) {
				            $services->the_post();
				            $list_ids[$control_post] = get_the_ID();



				            ?>

				            <?php


							$image = get_field('portada_de_joya');
							if( !empty( $image ) ): 
							?>                    
							      <div class="col-6 col-md-4 col-lg-3 d-flex justify-content-center">
									<div class="box-image bg-white mb-4 mb-lg-5 " onclick='openPhotoSwipe("<?php echo $image['url']?>","<?php echo $image['sizes'][ '2048x2048' . '-width' ].'X'.$image['sizes'][ '2048x2048' . '-height' ]?>")'>
										<img class="item-image-cat" src="<?php echo $image['url']?>">
										<p class="px-2 px-lg-3 text-center my-3 d-flex align-items-center"><?php 
										$description = the_field('text_es');
										if( strpos(get_locale(),'es') !== false ){				    	
											if(!empty($description)){
												$description = the_field('text_en');
											}
										}
										?></p>
									</div>
								</div>
							<?php else: ?>          
							      Sin elementos
							<?php endif; ?> 



				            <?php
				        }

				        wp_reset_postdata();
				    }
				?>

				

				
			</div>

		</div>
	</div>
</section>


<!-- SECTION GALERY -->
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <!-- <button class="pswp__button pswp__button--share" title="Share"></button> -->

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

           <!--  <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div> -->

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
<!-- END SECTION GALERY -->


