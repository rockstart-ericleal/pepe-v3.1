<!-- MENU ABSOLUTE -->

<div id="box-init" class="fixed-top v-100 h-full box-menu-absolute d-md-none d-none d-lg-block">
	
	<div class="row h-full">
		<div class="col-6">
			<div class="row h-full position-relative">
				
				<div class="col-12 p-0 align-self-center mt-5 text-center">					
					<?php echo get_the_post_thumbnail( $post_id, null, array( 'class' => 'text-logo main-logo ml-5 hh-auto' ) ); ?>
				</div>				
			</div>		
		</div>
	</div>
	
</div>






<div id="box-item-container" class="section cc-blog container-fluid no-gutters position-relative ">		
	
	<!-- SEMBLANZA PEPE 1 -->
	<section>
		<div class="row  ">
			<div class="col">
				<div class="row">
					<div class="col-md-12 col-lg-6 d-md-none d-none d-lg-block  item-view position-relative">
						
						<!-- MENU FLOAT -->

						<!-- SPACE -->

						<!-- END MENU FLOAT -->
						
					</div>
					<div class="col-md-12 col-lg-6 my-5 blog">
						<div class="row h-100p">
							<div class="col-12">
								<div class="col-12 text-center mt-5 pt-4 d-sm-block d-md-none">
									<h1 class="face-cr title-blog-page mb-4">BLOG</h1>
								</div>
								<div class="col-12 mb-5 align-self-center text-center d-block d-lg-none">
									
									<?php echo get_the_post_thumbnail( $post_id, null, array( 'class' => 'photo-pepe hh-auto' ) ); ?>
								</div>
							</div>
							<!-- <div class="col-12 d-none d-lg-block" style="height: 120px;"></div> -->
							<div class="col-12 text-center">

								<h1>
									<?php the_title( '<b>', '</b>' ); ?>										
								</h1>
								<div class="container-blog pt-3 blog-content">
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>									
										<?php the_content(); ?>
									<?php endwhile; ?>
									<?php endif; ?>
								</div>
							
							</div>

							<?php
							$next_post = get_next_post();
							if ( is_a( $next_post , 'WP_Post' ) ) : ?>							    

							    <div class="col-12 px-5 pt-5 box-next-post">
									<a href="<?php echo get_permalink( $next_post->ID ); ?>">
										<div class="row">
											<div class="col-6 d-flex justify-content-end">
												<?php echo get_the_post_thumbnail($next_post->ID, 'thumbnail'); ?>
											</div>
											<div class="col-6 d-flex align-self-center pl-0">
												<b><?php echo get_the_title( $next_post->ID ); ?></b>
											</div>
										</div>
									</a>
								</div>

							<?php else:?>
								<div class="col-12 px-5 pt-5 box-next-post">
									<a href="https://pepedavalos.com/como-limpiar-el-oro-blanco/">
										<div class="row">
											<div class="col-6 d-flex justify-content-end">
												<img src="https://pepedavalos.com/wp-content/uploads/2020/02/anillos-2-blog.png">
											</div>
											<div class="col-6 d-flex align-self-center pl-0">
												<b>Como limpiar el Oro Blanco</b>
											</div>
										</div>
									</a>
								</div>
								 
							<?php endif; wp_reset_query();?>

							<?php
							$previous_post = get_previous_post();
							if ( is_a( $previous_post , 'WP_Post' ) ) : ?>							    

							    <div class="col-12 px-5 pt-5 box-next-post">
									<a href="<?php echo get_permalink( $previous_post->ID ); ?>">
										<div class="row">
											<div class="col-6 d-flex justify-content-end">
												<?php echo get_the_post_thumbnail($previous_post->ID, 'thumbnail'); ?>
											</div>
											<div class="col-6 d-flex align-self-center pl-0">
												<b><?php echo get_the_title( $previous_post->ID ); ?></b>
											</div>
										</div>
									</a>
								</div>

							<?php else:?>
								<div class="col-12 px-5 pt-5 box-next-post">
									<a href="https://pepedavalos.com/como-elegir-una-pieza-de-joyeria-adecuada-para-cada-ocasion/">
										<div class="row">
											<div class="col-6 d-flex justify-content-end">
												<img src="https://pepedavalos.com/wp-content/uploads/2020/02/anillos-blog.png">
											</div>
											<div class="col-6 d-flex align-self-center pl-0">
												<b>¿Cómo elegir una pieza de joyería adecuada para cada ocasión?</b>
											</div>
										</div>
									</a>
								</div>
							<?php endif; wp_reset_query();?>


							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


